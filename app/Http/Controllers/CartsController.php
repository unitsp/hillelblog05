<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class CartsController extends Controller
{
    public function store(Product $product){
        $cart = request()->cookie('cart') ?
            json_decode(request()->cookie('cart'), true) : [];
        if(isset($cart[$product->id])) $cart[$product->id]++;
        else $cart[$product->id] = 1;
        return redirect('/products')
            ->withCookie('cart', json_encode($cart));
    }
}
