<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MembersController extends Controller
{
    public function __construct(){
//        $this->middleware('auth');
    }

    public function login(){
        return view('members.login');
    }

    public function store(){

       if(! auth()->attempt(request(['email','password']))){
            return back();
       }
       return redirect('/admin');

    }


    public function logout(){

        auth()->logout();
        return redirect('/member/login');
    }
}
