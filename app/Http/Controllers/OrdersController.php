<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    public function create(){
        $cart = json_decode(request()->cookie('cart'), true);
        $products = array();
        if(!$cart){
            return redirect('/products');
        }
        foreach($cart as $product_id => $amount){
            $products []= Product::find($product_id);
        }
        return view('orders.create',
            compact('cart','products'));
    }

    public function store(){
        $cart = json_decode(request()->cookie('cart'),
            true);
        $order = Order::create(request([
            'customer_name', 'phone', 'email', 'feedback'
            ]));
        foreach ($cart as $product => $amount){
            $order->products()->attach($product,
                ['amount'=> $amount]);
        }
        \Cookie::forget('cart');
    }
}
