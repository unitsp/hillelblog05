<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller
{
    public function index(){

        $data['posts'] = Post::all();
        return view('index', $data);

    }

    public function show(Post $post){

        return view('posts/show', compact('post'));

    }

    public function create(){
        return view('posts/create');
    }

    public function store(){

        $this->validate(request(),[
            'title' => 'required|string',
            'alias' => 'required|unique:posts',
            'intro' => 'required',
            'body' => 'required'
        ]);

        Post::create(request()->all());
        return redirect('/');

    }

    public function edit(Post $post){

        return view('posts/edit', compact('post'));

    }

    public function update(Post $post){
        $post->update(request()->all());
        $this->validate(request(),[
            'title' => 'required|string',
            'alias' => 'required',
            'intro' => 'required',
            'body' => 'required'
        ]);
        return redirect('/');
    }

    public function delete(Post $post){
        return view('posts/delete', compact('post'));
    }

    public function destroy(Post $post){
        $post->delete();
        return redirect('/');
    }

}
