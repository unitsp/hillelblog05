<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //Допустимые
    protected $fillable = ['title', 'alias','intro', 'body'];


    public function getRouteKeyName()
    {
        return 'alias';
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function addComment($body){
        Comment::create([
            'post_id' => $this->id,
            'body' => $body
        ]);
    }

}
