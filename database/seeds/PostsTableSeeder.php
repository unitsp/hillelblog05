<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            [
                'title' => 'Php is awesome',
                'alias' => 'php_is_awesome',
                'intro' => 'Php is awesome. Php is awesome. Php is awesome.',
                'body' => 'Php is awesome Php is awesome Php is awesome Php is awesome Php is awesome',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ],
            [
                'title' => 'Php is awesome1',
                'alias' => 'php_is_awesome1',
                'intro' => 'Php is awesome. Php is awesome. Php is awesome.',
                'body' => 'Php is awesome Php is awesome Php is awesome Php is awesome Php is awesome',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ],
            [
                'title' => 'Php is awesome2',
                'alias' => 'php_is_awesome2`',
                'intro' => 'Php is awesome. Php is awesome. Php is awesome.',
                'body' => 'Php is awesome Php is awesome Php is awesome Php is awesome Php is awesome',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ],
        ]);

    }
}
