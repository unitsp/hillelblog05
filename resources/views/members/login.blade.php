@extends('template')

@section('content')
    <form method="post" action="/member/login">
        {{csrf_field()}}
        <div class="form-group">
            <label for="email">Email:</label>
            <input type="email" name="email"
                   id="email" class="form-control">
        </div>
        <div class="form-group">
            <label for="password">Password:</label>
            <input type="password" name="password"
                   id="password" class="form-control">
        </div>
        <div class="form-group">
            <button class="btn btn-primary">Login</button>
        </div>
    </form>
@endsection