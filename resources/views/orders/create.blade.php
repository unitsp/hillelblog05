@extends('template')

@section('content')
    <div class="col-md-8">
        @if(isset($products)):

            @foreach($products as $product)
                <h2>{{$product->title}} x {{$cart[$product->id]}}</h2>
            @endforeach

            <form action="/order" method="post">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="customer_name">Enter your name:</label>
                    <input type="text" class="form-control"
                           name="customer_name" id="customer_name">
                </div>
                <div class="form-group">
                    <label for="phone">Enter your phone:</label>
                    <input type="text" class="form-control"
                           name="phone" id="phone">
                </div>
                <div class="form-group">
                    <label for="email">Enter your email:</label>
                    <input type="text" class="form-control"
                           name="email" id="email">
                </div>
                <div class="form-group">
                    <label for="feedback">Enter your feedback:</label>
                    <textarea class="form-control"
                              name="feedback" id="feedback"></textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-success"
                            type="submit">Create order</button>
                </div>
            </form>
        @endif
    </div>
@endsection