@extends('template')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2>Edit post:</h2>
                <form method="POST" action="/posts/{{$post->alias}}">
                    {{method_field('PUT')}}
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input value="{{$post->title}}" type="text" name="title" id="title" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="alias">Alias</label>
                        <input value="{{$post->alias}}" type="text" name="alias" id="alias" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="intro">Intro:</label>
                        <textarea name="intro" id="intro" class="form-control">{{$post->intro}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="body">Body:</label>
                        <textarea name="body" id="body" class="form-control">{{$post->body}}</textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary"> Save </button>
                    </div>

                    @include('layout/errors')

                </form>
            </div>
        </div>
    </div>

@endsection