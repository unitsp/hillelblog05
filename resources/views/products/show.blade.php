@extends('template')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-8 col-offset-2">
                <h1>{{ $product->title }}</h1>
                <p>Price : {{ $product->price }}</p>
                <p>{{ $product->description }}</p>
                <p><a class="btn btn-success"
                      href="/cart/{{$product->alias}}">Buy</a></p>
            </div>

        </div>
    </div>
@endsection