<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PostsController@index');

Route::get('/posts/create', 'PostsController@create');

Route::get('/posts/{post}', 'PostsController@show');

Route::post('/posts', 'PostsController@store');

Route::get('/posts/{post}/edit', 'PostsController@edit');

Route::put('/posts/{post}', 'PostsController@update');

Route::get('/posts/{post}/delete', 'PostsController@delete');

Route::delete('/posts/{post}', 'PostsController@destroy');

Route::post('/posts/{post}/comment','CommentsController@store');

Route::get('/products', 'ProductsController@index');

Route::get('/products/{product}' , 'ProductsController@show');

Route::get('/cart/{product}', 'CartsController@store');

Route::get('/order', 'OrdersController@create');

Route::post('/order', 'OrdersController@store');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin', [
    'middleware' => 'auth',
    'uses' => 'Admin\OrdersController@index'
]);

Route::get('/admin/test', 'Admin\OrdersController@test');

Route::get('/member/login', 'MembersController@login');

Route::post('/member/login', 'MembersController@store');

Route::get('/member/logout', 'MembersController@logout');